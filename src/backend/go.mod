module backend

go 1.12

require (
	github.com/DataDog/zstd v1.4.4 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/rs/cors v1.7.0 // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.4.4
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
)
