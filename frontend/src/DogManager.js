import React, { useCallback, useState } from "react";
import "./DogManager.css";

export default ({ dogs = [], onCreateNewDog }) => {
  const [newDogName, setNewDogName] = useState("");

  const onNewDogNameChange = useCallback((event) => {
    setNewDogName(event.target.value);
  }, []);

  const onAddDoggie = useCallback(
    (event) => {
      event.preventDefault();
      onCreateNewDog(newDogName);
      setNewDogName("");
    },
    [onCreateNewDog, newDogName]
  );

  return (
    <div className="container">
      <section className="dog-create-form">
        <form>
          <input
            type="text"
            placeholder="New Message"
            value={newDogName}
            onChange={onNewDogNameChange}
          />
          <button type="submit" onClick={onAddDoggie}>
            Save Message
          </button>
        </form>
      </section>
      <div className="divider" />
      <section className="dog-list-section">
        <p>My messages:</p>
        <ul>
          {dogs.map((dog) => (
            <li key={dog.message}>{dog.message}</li>
          ))}
        </ul>
      </section>
    </div>
  );
};