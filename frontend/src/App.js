
import './App.css';
import MenuAppBar from './MenuAppBar';
import Message from './Message';
function App() {
  return (
    <div className="App">
      <MenuAppBar/>
      <Message/>
    </div>
  );
}

export default App;
