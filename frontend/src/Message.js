import React, { useEffect, useState, useCallback } from "react";
import './Message.css';
import axios from 'axios';
import DogManager from "./DogManager";
// import Table from "./Table";

const myDogServerBaseURL = "http://127.0.0.1:54754";

const Message = () => {
  //This state will be populated with the Axios HTTP response
  const [dogs, setDogs] = useState([]);

  const loadDogs = useCallback(() => {
    axios.get(`${myDogServerBaseURL}/messages`).then((response) => {
      setDogs(response.data);
    });
  }, []);

  //The doggies are loaded initially
  useEffect(() => {
    loadDogs();
  }, [loadDogs]);

  const onCreateNewDog = useCallback(
    (newDogName) => {
      axios
        .post(`${myDogServerBaseURL}/messages`, {
          author: 'Leela',
          message: newDogName,
        })
        .then((result) => {
          //Reload the doggies to show also the new one
          loadDogs();
        })
        .catch((error) => {
          /**
           * If the response status code is 409 - Conflict, then we already have
           * a doggie with this name.
           **/
          if (error.response.status === 409) {
            alert("Doggie with same name exists!");
          } else {
            alert("Unknown error.");
          }
        });
    },
    [loadDogs]
  );

  return (
    <main className="u-centered-content u-full-vw u-full-vh">
      <DogManager dogs={dogs} onCreateNewDog={onCreateNewDog} />
      {/* <Table dogs={dogs}/> */}
    </main>
  );
};

export default Message;
