import * as React from 'react';
import { DataGrid } from '@material-ui/data-grid';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'author', headerName: 'Author', width: 130 },
  { field: 'message', headerName: 'Message', width: 130 },
  {
    field: 'date',
    headerName: 'date',
    type: 'date',
    width: 90,
  },
//   {
//     field: 'fullName',
//     headerName: 'Full name',
//     description: 'This column has a value getter and is not sortable.',
//     sortable: false,
//     width: 160,
//     valueGetter: (params) =>
//       `${params.getValue('firstName') || ''} ${params.getValue('lastName') || ''}`,
//   },
];

export default ({ dogs = []}) =>  {
  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid rows={dogs} columns={columns} pageSize={5} checkboxSelection />
    </div>
  );
}
